# SpringBoot演示项目

## 介绍

SpringBoot演示项目，进行基础的开发讲解Demo，一点点深入SpringBoot开发原理。

![SpringBootDemo](spring-boot-demo.png)


## 软件架构

|       软件/组件    |    版本    |
|  --------------   | --------- |
| JDK               | 17+       |
| Maven             | 3.8.6+    |
| MySQL             | 8.0.31+   |
| SpringBoot        | 3.0.1+    |
| Log4j2            | 2.19.0    |
| Lombok            | 1.18.24   |
| mysql-connector   | 8.0.31    |

## 更新说明

|  时间    |   更新内容   |
|  -----   |   -----    |
| 2022-12-18 | 升级到SpringBoot 3.0  |
| 2022-08-22 | MySQL Connector Java添加  |
| 2022-08-21 | Lombok添加  |
| 2022-08-13 | Log4j2日志添加   |
| 2022-08-12 | 框架搭建   |


## 使用说明

### 1. 环境搭建
[Java环境配置/JDK安装配置](https://jackwei.blog.csdn.net/article/details/86550186) \
[Maven的安装+配置本地仓库路径](https://jackwei.blog.csdn.net/article/details/93717865) \
[MySQL8.0和5.7安装教程](https://jackwei.blog.csdn.net/article/details/86908034)


### 2. 视频教程
相应的视频教程可以到博主的B站查看：[Jack魏1996](https://space.bilibili.com/476515241/)

## 参与贡献
[码云/GitHub Fork代码仓并提交PR代码](https://jackwei.blog.csdn.net/article/details/127661721)
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
