/*
 * Copyright © Jack魏 2022 - 2022 , All Rights Reserved.
 */

package com.jack.demo.bean;

import lombok.Data;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Lombok测试
 *
 * @author Jack魏
 * @since 2022-08-21
 */
@Data
class LombokTest {
    private String name;
    private Integer age;

    @Test
    void testGetter() {
        LombokTest test = new LombokTest();
        Integer age = test.getAge();
        Assertions.assertNull(age, " expected: null ");
    }

    @Test
    void testSetter() {
        LombokTest test = new LombokTest();
        test.setAge(18);
        Integer age = test.getAge();
        Assertions.assertNotNull(age, " expected: not null ");
    }
}
