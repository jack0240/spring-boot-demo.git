/*
 * Copyright © Jack魏 2022 - 2022 , All Rights Reserved.
 */

package com.jack.demo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 测试主类
 *
 * @author Jack魏
 * @since 2022-08-12
 */
@SpringBootTest
class SpringBootDemoApplicationTests {

    /**
     * 测试方法
     */
    @Test
    void contextLoads() {
        SpringBootDemoApplication application = new SpringBootDemoApplication();
        Assertions.assertNotNull(application);
    }

}
