/*
 * Copyright © Jack魏 2022 - 2022 , All Rights Reserved.
 */

package com.jack.demo.db;

import com.jack.demo.bean.Authors;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * MySQL数据库操作测试
 *
 * @author Jack魏
 * @since 2022-08-21
 */
public class JdbcTest {
    /**
     * 连接地址
     */
    public static final String URL = "jdbc:mysql://127.0.0.1:3306/spring-boot-demo";

    /**
     * 用户名
     */
    public static final String USER = "root";

    /**
     * 密码
     */
    public static final String PASSWORD = "123456";

    /**
     * 连接
     */
    private static Connection conn = null;

    static {
        try {
            //1.加载驱动程序
            Class.forName("com.mysql.jdbc.Driver");
            //2. 获得数据库连接
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 维护常量连接，不用每次都创建
     */
    public static Connection getConnection() {
        return conn;
    }

    /**
     * 测试查询
     *
     * @throws SQLException
     */
    @Test
    void testQuery() throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM authors limit 10");

        List<Authors> authorsList = new ArrayList<Authors>();
        Authors author = null;
        while (rs.next()) {
            author = new Authors();
            author.setId(rs.getInt("id"));
            author.setFirstName(rs.getString("first_name"));
            author.setLastName(rs.getString("last_name"));
            author.setEmail(rs.getString("email"));
            author.setBirthdate(rs.getDate("birthdate"));
            author.setAdded(rs.getDate("added"));
            System.out.println(author);
            authorsList.add(author);
        }
        Assertions.assertNotNull(author, "expected not null");
    }
}
