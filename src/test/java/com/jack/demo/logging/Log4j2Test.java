/*
 * Copyright © Jack魏 2022 - 2022 , All Rights Reserved.
 */

package com.jack.demo.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Log4j2日志测试
 *
 * @author Jack魏
 * @since 2022-08-13
 */
class Log4j2Test {
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * 测试Log4j2 日志输出
     */
    @Test
    void testConsolePrintTest() {
        String thing = "world";
        Assertions.assertNotNull(thing, "thing is null");
        LOGGER.debug("Got calculated value only if debug enabled: {}", System.currentTimeMillis());
        LOGGER.info("Hello, {}!", thing);
        LOGGER.warn("This is Warn......");
        LOGGER.error("Error---------");
    }

}
