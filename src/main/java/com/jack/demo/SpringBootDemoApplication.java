/*
 * Copyright © Jack魏 2022 - 2022 , All Rights Reserved.
 */

package com.jack.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration;

/**
 * 启动类
 *
 * @author Jack魏
 * @since 2022-08-12
 */
@SpringBootApplication(exclude = {
    MultipartAutoConfiguration.class,
    JmxAutoConfiguration.class,
})
public class SpringBootDemoApplication {

    /**
     * 主方法
     *
     * @param args 命令参数
     */
    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoApplication.class, args);
    }

}
