/*
 * Copyright © Jack魏 2022 - 2022 , All Rights Reserved.
 */

package com.jack.demo.bean;

import lombok.Data;

import java.util.Date;

/**
 * 作者实体类
 *
 * @author Jack魏
 * @since 2022-08-21
 */
@Data
public class Authors {
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private Date birthdate;
    private Date added;
}
